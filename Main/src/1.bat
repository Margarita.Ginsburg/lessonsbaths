@echo off
if exist file.txt (erase file.txt)
:again
<nul set /p x=#>>file.txt
for %%A IN (file.txt) DO (
 echo.%%~zA
 if %%~zA GEQ 100 (goto :out)
)
goto :again
:out
erase file.txt
pause>nul
exit /b